function checkInp(){
    let inp = document.getElementById("kol").value;
    if(inp.match(/^[0-9]+$/) === null){
        document.getElementById("price").innerHTML = "Неверные данные";
        return false;
    }else{
        return true;
    }
}
function getPrices() {
    return {
        Complcar: [1000000, 1270000, 1500000],
        prodOptions: {
            option1 : 0,
            option2: 50000,
            option3: 127000,
        },
        prodProperties: {
            prop1: 20000,
            prop2: 127000,
        },
    };
}
function price(){
    let sel = document.getElementById("Complcar");
    let kol = document.getElementById("kol").value;
    let radioDiv = document.getElementById("radios");
    let boxes = document.getElementById("checkboxes");
    let price = document.getElementById("price");
    price.innerHTML = "0";
    switch (sel.value) {
        case "1":
            radioDiv.style.display = "none";
            boxes.style.display = "none";

            if(checkInp()){
                let all = parseInt(kol) * getPrices().Complcar[0];
                price.innerHTML = all + " рублей";
            }
            break;
        case "2":
            radioDiv.style.display = "block";
            boxes.style.display = "none";
            let a;
            let rad = document.getElementsByName("prodOptions");
            for (var i = 0; i < rad.length; i++) {
                if (rad[i].checked){
                    a = rad[i].value;
                }
            }
            let priOfOp = getPrices().prodOptions[a];
            if(checkInp()){
                let all = parseInt(kol)*(getPrices().Complcar[1] + priOfOp);
                price.innerHTML = all + " рублей";
            }
            break;
        case "3":
            radioDiv.style.display = "none";
            boxes.style.display = "block";
            let sum = 0;
            let p = document.getElementsByName("prop");
            for (let i = 0; i < p.length ; i++) {
                if(p[i].checked){
                    sum += getPrices().prodProperties[p[i].value];
                }
            }

            if(checkInp()){
                let all = parseInt(kol) * (getPrices().Complcar[2] + sum);
                price.innerHTML = all + " рублей";
            }
            break;
    }
}
window.addEventListener("DOMContentLoaded", function (event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    let boxes = document.getElementById("checkboxes");
    boxes.style.display = "none";

    let kol = document.getElementById("kol");
    kol.addEventListener("change", function (event) {
        console.log("Kol was changed");
        price();
    });

    let select = document.getElementById("Complcar");
    select.addEventListener("change", function (event) {
        console.log("Model was changed");
        price();
    });

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            console.log("Engine was changed");
            price();
        });
    });
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function (event) {
            console.log("Options was changed");
            price();
        });
    });
    price();
});